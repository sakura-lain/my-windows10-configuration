# My Windows 10 Configuration

**My custom configuration for Windows 10 : tiles, time zone...**
- The laptop is an Asus ROG Strix Scar II model GL704GV_G715GV.
- Windows 10 version is Windows 10 Pro 10.0.1.18362 (1909).

Beware! This tutorial mostly provides tips to backup the graphical appearance of Windows 10 as well as a few custom settings. Its goal is to gain time in case I have to reinstall it. It does not provide privacy tips. The privacy policy can be set pu through the Privacy Settings (Paramètres de confidentialité). For a detailed tutorial about privacy settings, please check:
- https://github.com/adolfintel/Windows10-Privacy

## Tiles backup

![](./Pictures/2020-04-15_114730_WinTiles.png)

There are at least two ways to back up the tiles.

### 1st method (the simplest)

Open PowerShell in admin mode and type:
```
Export-StartLayout -path $env:LOCALAPPDATA\LayoutModification.xml
```
This will create a `LayoutModification.xml` file in the user's `AppData\Local` hidden folder. Full path:
```
%USERPROFILE%\AppData\Local
```
To restore this configuration on a new user profile:
```
If((Test-Path $env:LOCALAPPDATA\LayoutModification.xml) -eq $True) {
Copy-Item $env:LOCALAPPDATA\LayoutModification.xml $env:LOCALAPPDATA\Microsoft\Windows\Shell\LayoutModification.xml -Force
Remove-Item 'HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudStore\Store\Cache\DefaultAccount\*$start.tilegrid$windows.data.curatedtilecollection.tilecollection' -Force -Recurse
Get-Process Explorer | Stop-Process
}
```
Source : https://www.addictivetips.com/windows-tips/back-up-start-menu-without-tiledatalayer-folder-on-windows-10/

### 2nd method (with the registry entry)

Backup:
- With regedit, export and save the key `HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\CloudStore\Store\Cache\DefaultAccount` (see below for a full explanation)
- Backup the file `%LocalAppData%\Microsoft\Windows\Shell\DefaultLayouts.xml`
- Put the key and the file in the same folder for convenience

Restoration:
- With regedit, delete the current key `HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\CloudStore\Store\Cache\DefaultAccount`
- Replace it by merging the previoulsky saved corresponding key in the registry
- Replace the current `%LocalAppData%\Microsoft\Windows\Shell\DefaultLayouts.xml` by the previously saved one
- Sign out and then sign in again

Source : https://winaero.com/blog/backup-restore-start-menu-layout-windows-10/

## Taskband backup

![](./Pictures/2020-04-15_115656_taskband.png)

To back up the taskband, follow the following steps:
- First, open `regedit` with the key combination `Win+R`
- Look for `Computer\HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Taskband`
- On the left panel, right clic on `taskband` then choose `Export` in the context panel
- Choose the backup location and name the `.reg` backup file

Source : https://www.justgeek.fr/sauvegarder-la-disposition-de-la-barre-des-taches-sous-windows-10-60593/

## Full registry backup

To back up the whole registry, follow the same steps but chose `Computer` at steps 2 and 3.

## Time management

In case of a dual boot with Linux, Windows usually loses the right time. This is because Windows uses local time then saves it on the motherboard. On the contrary, Linux uses UTC then adds an offset depending on the current location.

To correct this problem, Windows should use UTC too, instead of local time. This can be done by changing a simple registry entry:

- First, open `regedit` with the key combination `Win+R`
- Look for `HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\TimeZoneInformation`
- Create a DWORD 32bits key named `RealTimeIsUniversal`
- Put its value to `1`

![](./Pictures/2020-04-14_182735_TimeZone.png)

Source : https://www.numetopia.fr/dual-boot-corriger-difference-heure-windows-linux/

## Taking back control in case a corporate account is linked to Windows and demands privileged rights

Depending of the company you're working for and its privacy policy and assuming that the said company provides a corporate licence for a professional use of Microsoft tools such as Office 365, OneDrive, Skype Enterprise, etc. preventing it from taking control over your compute, for instance by imposing a strong password for the user account or demanding that you crypt your hard drive, can be tricky. If it happens, you can try those steps :
- If you're using a corporate version of OneDrive, disconnect it and remove the files, especially if they're registered as belonging to your company and not as personal files (not sure if it's fully necessary)
- Uninstall any Microsoft software provided via a corporate licence (idem)
- Disconnect any corporate account from the computer by removing it from any domain it could be connected to (very important, see below)
- Reinstall your corporate softwares making sure to limit your coroporate account access to those softwares only (this can be done by checking the appropriate option before installing, for instance, Office 365 under a corporate licence)

Removing a computer from a domain can be done in two ways:
- Through Settings > Accounts > Access work or school > Disconnect [Paramètres > Comptes > Accès professionnel ou scolaire > Déconnecter]
- Through Control Panel (icon view) > System > Advanced system settings (or Change settings) > Computer name tab > select Workgroup > OK [(Panneau de configuration (vue en icônes) > Système > Paramètres système avancés (ou Modifier les paramètres) > onglet Nom de l'ordinateur > sélectionner Workgroup > OK]
- Reboot

Source : https://www.tenforums.com/tutorials/90107-remove-windows-10-pc-domain.html
